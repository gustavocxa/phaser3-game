export const CST = {

    SCENES: {
        LOAD: "LOAD",
        MENU: "MENU",
        PLAY: "PLAY"
    },

    IMAGE: {
        PHASER3: "phaser3.png",
        LOGO: "logo.png",
        OPTIONS: "options_button.png",
        PLAY: "play_button.png",
        TITLE: "title_bg.jpg",
        BOMB: "bomb.png",
        HEAR: "corazon.png",
        PLATFORM: "platform.png",
        SKY: "sky.png",
        STAR: "star.png",
        SUELO: "suelo.png",
        SCREEN: "screenSize.png"
    },

    BUTTONS: {
        LEFT: "buttonLeft.svg",
        RIGHT: "buttonRight.svg",
        UP: "buttonUp.svg",
        DOWN: "buttonDown.svg",
    },

    AUDIO: {
        MUSIC: "music.ogg",
        BOMB: "bomb.ogg",
        COIN: "coin.ogg",
        DARVIDA: "darVida.ogg",
        GAMEOVER: "gameOver.ogg",
        JUMP: "jump.ogg",
    },

    SPRITE: {
        SCREENSIZE: "screenSize.png"
    },

    SVG: {
        BACKGROUND: "scene1.svg",
        HEAR: "hear.svg",
        PAUSE: "buttonPause.svg"
    },

    PLAYER:{
     BOY: "boy.png" 
    }

}