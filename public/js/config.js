/** @type {import("../lib/typings/phaser")} */

import { LoadScene } from "./LoadScene.js";
import { PlayScene } from "./PlayScene.js";

let game = new Phaser.Game({

    type: Phaser.AUTO,

    width: 1366,
    height: 768,

    autoResize: true,

    audio: { disableWebAudio: true },

    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },

    render: {
        pixelArt: true
    },

    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },

    scene: [
        LoadScene, PlayScene
    ]

})